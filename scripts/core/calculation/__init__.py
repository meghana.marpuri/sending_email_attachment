import pandas as pd


def calculation():
    file1 = pd.read_csv('E:/email/data.csv')
    file = file1.set_index('Timestamp')
    file.head(10)
    max_value_timestamp = file.idxmax().to_dict()
    min_value_timestamp = file.idxmin().to_dict()
    max_column_values = file.max().to_dict()
    min_column_values = file.min().to_dict()
    max_dict = {}
    min_dict = {}
    for k in max_value_timestamp.keys():
        max_dict[k] = (max_column_values[k], max_value_timestamp[k])
    for k in min_value_timestamp.keys():
        min_dict[k] = (min_column_values[k], min_value_timestamp[k])
    max_series = pd.DataFrame.from_dict(max_dict, orient='index',columns=['Max', 'Timestamp'])
    min_series = pd.DataFrame.from_dict(min_dict, orient='index',columns=['Min', 'Timestamp'])
    avg = file.mean(axis=0)
    return max_series, min_series, avg
