import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.application import MIMEApplication
from scripts.core.calculation import calculation


def send_test_mail(recipients):
    sender_email = "meghana.marpuri@knowledgelens.com"
    max_val, min_val, avg = calculation()
    min_data = min_val.to_html()
    max_data = max_val.to_html()
    msg_body = """\
    <html>
        <body> 
            <p>
                Min Max and Avg of attached file data: <br>
                Minimum values of data: <br>
                """+min_data+"""
                <br>
                Maximum values of data: <br>
                """+max_data+"""
                <br>
                Average values of data: <br>"""+str(avg)+"""<br>
            </p>
            <p> message </p>
        </body> 
    </html> """
    msg = MIMEMultipart()
    msg['Subject'] = '[Email Test]'
    msg['From'] = sender_email
    msg['To'] = ', '.join(recipients.split(','))
    filename = 'E:\\email\\data.csv'
    msg.attach(MIMEText(msg_body, 'html'))
    with open(filename, 'rb') as csvfile:
        msg.attach(MIMEApplication(csvfile.read(), Name="data.csv"))

    smtpobj = smtplib.SMTP('smtp.gmail.com', 587)
    smtpobj.starttls()
    passkey = input("Password: ")
    smtpobj.login("meghana.marpuri@knowledgelens.com", passkey)
    smtpobj.send_message(msg)
    smtpobj.quit()
