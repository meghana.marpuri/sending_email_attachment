from scripts.core.calculation import calculation
from scripts.core.send_mail import send_test_mail


if __name__ == '__main__':
    max_val, min_val, avg = calculation()
    send_test_mail('nandini.kopparthi@knowledgelens.com,yasaswi.katakam@knowledgelens.com')
